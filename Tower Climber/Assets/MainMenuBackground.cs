using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuBackground : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnGameStart()
    {
        gameObject.SetActive(false);
    }
    private void OnGameRestart()
    {
        gameObject.SetActive(true);
    }
    private void OnEnable()
    {
        UIManager.onGameStart += OnGameStart;
        UIManager.onGameRestart += OnGameRestart;
    }

    private void OnDestroy()
    {
        UIManager.onGameStart -= OnGameStart;
        UIManager.onGameRestart -= OnGameRestart;
    }

    
}
