using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : Singleton<MusicPlayer>
{
    [SerializeField] AudioClip menuMusic;
    [SerializeField] AudioClip gameMusic;
    AudioSource audioSource;

    protected override void Awake()
    {
        base.Awake();
        audioSource = GetComponent<AudioSource>();
    }

    private void OnGameStart()
    {
        audioSource.clip = gameMusic;
        audioSource.Play();
    }

    private void OnGameRestart()
    {
        audioSource.clip = menuMusic;
        audioSource.Play();
    }

    private void OnPlayerDeath()
    {
        audioSource.Stop();
    }
    private void OnGameContinue()
    {
        audioSource.Play();
    }
    private void OnEnable()
    {
        UIManager.onGameContinue += OnGameContinue;
        UIManager.onGameStart += OnGameStart;
        UIManager.onGameRestart += OnGameRestart;
        KillCollider.onPlayerDeath += OnPlayerDeath;
    }

    private void OnDisable()
    {
        UIManager.onGameContinue -= OnGameContinue;
        UIManager.onGameStart -= OnGameStart;
        UIManager.onGameRestart -= OnGameRestart;
        KillCollider.onPlayerDeath -= OnPlayerDeath;
    }

    
}
