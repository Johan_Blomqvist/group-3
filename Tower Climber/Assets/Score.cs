using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{

    
    public Transform player;
    
    public Text scoreText;
    private int score = 0;

    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.transform.tag == "Coin")
        {
            
            collision.gameObject.SetActive(false);
            score++;
            scoreText.text = score.ToString();
        }
    }
}
