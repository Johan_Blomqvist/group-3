using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Platform : MonoBehaviour
{
    AudioSource audioSource;
    [SerializeField] AudioClip audioClip;
    public float jumpForce = 12f;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }
    void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.relativeVelocity.y <= 0f )
        {
            Rigidbody2D rb = collision.collider.GetComponent<Rigidbody2D>();
            if (rb != null)
            {
                Vector2 velocity = rb.velocity;
                velocity.y = jumpForce;
                rb.velocity = velocity;
                audioSource.pitch = (Random.Range(1, 3));
                audioSource.PlayOneShot(audioClip);
            }
        }


        

    }
    
}
