using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Monetization;

namespace GameServices
{
    public class AdService : MonoBehaviour, IServices
    {
        private string placementID_Rewarded = "rewardedVideo";
        private string androidGameId = "4014051";
        private bool testmode = true;

        public void DisplayRewardedAd()
        {
            StartCoroutine(WaitAndDisplayRewardedAd());
        }

        public IEnumerator WaitAndDisplayRewardedAd()
        {
            Debug.Log("is working");
            yield return new WaitUntil(() => Monetization.IsReady(placementID_Rewarded));

            ShowAdPlacementContent adContent = Monetization.GetPlacementContent(placementID_Rewarded) as ShowAdPlacementContent;


            ShowAdCallbacks callbackOptions = new ShowAdCallbacks();
            callbackOptions.finishCallback += RewardCallback;

            if (adContent != null)
                adContent.Show(callbackOptions);
            else
                Debug.LogWarning("Placement content returned null.");
        }

        private void RewardCallback(ShowResult result)
        {
            switch(result)
            {
                case ShowResult.Finished:
                    Debug.Log("Rewards");
                    break;
                case ShowResult.Skipped:
                    Debug.Log("Ad Skipped, no rewards");
                    break;
                case ShowResult.Failed:
                    Debug.Log("Ad failed");
                    break;
                    
            }

        }

        public void Initialize()
        {
#if UNITY_IOS || UNITY_ANDROID
            Monetization.Initialize(androidGameId, testmode);
#else
            Debug.LogWarning("The current platform does not support Unity Monetization");
#endif
        }
    }

    
}
