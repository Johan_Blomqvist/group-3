using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraManager : MonoBehaviour
{
    public Transform player;

    public float smoothSpeed = .3f;
    public Vector3 offset;

    private void Update()
    {

    }
    private void LateUpdate()
    {
        if (player.position.y > transform.position.y)
        {
            Vector3 newPos = new Vector3(player.position.x, player.position.y, player.position.z);
            transform.position = newPos + offset ;
        }
    }
}
