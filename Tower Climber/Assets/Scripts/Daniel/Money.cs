using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Analytics;

public class Money : Singleton<Money>, IUnityAdsListener
{
    string Android_ID = "4014051";
    string myPlacementid = "rewardedVideo";
    bool TestMode = true;
    public static Events.RewardedAdEvent OnRewarded;

    // Start is called before the first frame update1
    void Start()
    {
        Advertisement.AddListener(this);
        Advertisement.Initialize(Android_ID, TestMode);
    }
    public void DisplayVideoAd()
    {
        Advertisement.Show(myPlacementid);
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        if(showResult == ShowResult.Finished)
        {
            if(OnRewarded != null)
            {
                OnRewarded(Enums.AdRewards.Rewarded);
            }

            AnalyticsResult analyticsResult = Analytics.CustomEvent("AdFinished", new Dictionary<string, object>
            {
                {"AdFinished", showResult }
            });
        }
        else if(showResult == ShowResult.Skipped)
        {
            if (OnRewarded != null)
            {
                OnRewarded(Enums.AdRewards.Skipped);
            }
        }
        else if(showResult == ShowResult.Failed)
        {
            if (OnRewarded != null)
            {
                OnRewarded(Enums.AdRewards.Failed);
            }
        }
    }
    public void OnUnityAdsReady(string placementId)
    {
        if(placementId == myPlacementid)
        {
            
        }
    }

    public void OnUnityAdsDidError(string message)
    {
        Debug.LogWarning("Ad got an error");
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        AnalyticsResult analyticsResult = Analytics.CustomEvent("AdStarted", new Dictionary<string, object>
        {
            {"AdStarted", placementId }
        });
    }
}
 
