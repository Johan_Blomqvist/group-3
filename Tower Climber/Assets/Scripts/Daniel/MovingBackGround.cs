using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovingBackGround : MonoBehaviour
{
    public Renderer bk1;
    public Renderer bk2;
    public Renderer bk3;

    public GameObject ground;

    private void Update()
    {
        if(bk1.isVisible && bk2.isVisible)
        {
            bk3.transform.position = bk1.transform.position + bk1.transform.up * 235f;

            ground.transform.position = bk2.transform.position + bk2.transform.up * -110f;
        }

        if(bk2.isVisible && bk3.isVisible)
        {
            bk1.transform.position = bk2.transform.position + bk2.transform.up * 235f;

            ground.transform.position = bk3.transform.position + bk3.transform.up * -110f;
        }

        if(bk3.isVisible && bk1.isVisible)
        {
            bk2.transform.position = bk3.transform.position + bk3.transform.up * 235;

            ground.transform.position = bk3.transform.position + bk3.transform.up * -110f;
        }
    }
}
