using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platforms_Test : MonoBehaviour
{
    public float jumpForce = 10f;

    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.relativeVelocity.y <= 0f)
        {
            Rigidbody2D rb2d = collision.collider.GetComponent<Rigidbody2D>();
            if (rb2d != null)
            {
                Vector2 velocity = rb2d.velocity;
                velocity.y = jumpForce;
                rb2d.velocity = velocity;
            }
        }
    }

}
