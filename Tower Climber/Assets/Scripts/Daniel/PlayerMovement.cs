using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float jumpHeight = 50f;
    public float moveSpeed = 100;

    //public Joystick joystick;


    private bool isGrounded;
    private Rigidbody2D rb2d;
    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        Invoke("Jumping", 2);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        isGrounded = true;
    }
    public void Movement()
    {
        Vector3 movement = new Vector3(Input.GetAxis("Horizontal") , 0f, 0f);
        transform.position += movement * Time.deltaTime * moveSpeed;
    }


    private void Jumping()
    {
        if (isGrounded == true)
        {
            rb2d.AddForce(new Vector2(0, jumpHeight), ForceMode2D.Impulse);
            isGrounded = false;
        }
    }
}
