using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameServices
{
    public class ServiceManager : MonoBehaviour
    {
        public static ServiceManager instance = null;

        private List<IServices> serviceManagers = new List<IServices>();

        // Start is called before the first frame update
        void Start()
        {
            if (instance == null)
                instance = this;
            else if (instance != null) 
                Destroy(gameObject);

            DontDestroyOnLoad(gameObject);
            IntitialiseServices();
        }

        private void IntitialiseServices()
        {
            foreach(IServices service in serviceManagers)
            {
                service.Initialize();
            }
        }
    }
}
