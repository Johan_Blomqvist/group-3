using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMovement : MonoBehaviour
{
    public Joystick joyStick;
    public float moveSpeed;

    private float hMove;
    private Rigidbody2D rb2d;

    private void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        Movement();
    }

    private void Movement()
    {
        if(joyStick.Horizontal >= .2f)
        {
            hMove = moveSpeed;
        }
        else if(joyStick.Horizontal <= -.2f)
        {
            hMove = -moveSpeed;
        }
        else
        {
            hMove = 0f;
        }

    }
    


  
}
