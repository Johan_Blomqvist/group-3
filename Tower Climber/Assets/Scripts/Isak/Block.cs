using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{

    Rigidbody2D rigidbody2D;
    [SerializeField][Range (0.2f, 10)] float fallTimer;

    private void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        StartCoroutine(DropBlockAfterTime());
        
    }

    private IEnumerator DropBlockAfterTime()
    {
        yield return new WaitForSeconds(fallTimer);
        rigidbody2D.bodyType = RigidbodyType2D.Dynamic;
    }
}
