using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScroll : MonoBehaviour
{
    [SerializeField] SpriteRenderer backgroundImage;
    Transform player;
    [SerializeField] float CameraCenterToPlayerDifference = 10;

    // Start is called before the first frame update
    void Awake()
    {
        player = FindObjectOfType<Player>().transform;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 temp = transform.position;
        temp.y = player.position.y + CameraCenterToPlayerDifference;
        transform.position = temp;
        backgroundImage.transform.position = new Vector3(transform.position.x, transform.position.y, 1);
        temp.y -= CameraCenterToPlayerDifference * 2;
    }
}
