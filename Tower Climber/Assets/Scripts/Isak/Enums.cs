using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enums
{
    public enum GameStates
    {
        Pregame,
        Playing,
        Paused
    }

    public enum PlayerInputs
    {
        Move
    }

    public enum MainMenuChoice
    {
        Store,
        Settings,
        Play
    }

    public enum GameOverChoice
    {
        WatchAd,
        QuitRun
    }

    public enum AdRewards
    {
        Rewarded,
        Skipped,
        Failed
    }
}
