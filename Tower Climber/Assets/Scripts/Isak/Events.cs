using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Events
{
    //public delegate void LoadNewLevel(string level);
    public delegate void InputEvent(Enums.PlayerInputs playerInputs);
    public delegate void MainMenuEvent(Enums.MainMenuChoice mainMenuChoice);
    public delegate void GameOverMenuEvent(Enums.GameOverChoice gameOverChoice);
    public delegate void EmptyEvent();
    public delegate void RewardedAdEvent(Enums.AdRewards adRewards);
    public delegate void PauseButtonEvent(bool pauseGame);
    public delegate void GameStateChangedEvent(Enums.GameStates gameState);
    public delegate void ScoreUpdateEvent(int score);
    public delegate void LeaderboardEvent(List<int> list);
}
