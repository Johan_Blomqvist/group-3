using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillCollider : MonoBehaviour
{

    public static Events.EmptyEvent onPlayerDeath;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Player temp = collision.gameObject.GetComponent<Player>();
        if (temp != null)
        {
            if(onPlayerDeath != null)
            {
                onPlayerDeath();
            }
        }
    }
}
