using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{

    public static Events.GameStateChangedEvent onGameStateChange;

    public Enums.GameStates currentGameState = Enums.GameStates.Pregame;

    [SerializeField] string gameSceneName;

    [Header ("Enable when developing and testing the game")]
    [SerializeField] bool debugMode = false;
    

    string currentLevel = string.Empty;

    void StartGame()
    {
        loadLevel(gameSceneName);
        UpdateGameState(Enums.GameStates.Playing);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void UpdateGameState(Enums.GameStates gameState)
    {
        switch(gameState)
        {
            case Enums.GameStates.Pregame:
                currentGameState = Enums.GameStates.Pregame;
                Time.timeScale = 0;
                break;

            case Enums.GameStates.Playing:
                currentGameState = Enums.GameStates.Playing;
                Time.timeScale = 1;
                break;

            case Enums.GameStates.Paused:
                currentGameState = Enums.GameStates.Paused;
                Time.timeScale = 0;
                break;
        }
        if(onGameStateChange != null)
        {
            onGameStateChange(currentGameState);
        }
    }

    void loadLevel(string level)
    {
        if(SceneManager.GetSceneByName(level) != null)
        {
            if (debugMode)
            {
                SceneManager.LoadSceneAsync(level);
            }
            else
            {
                SceneManager.LoadSceneAsync(level, LoadSceneMode.Additive);
            }
            currentLevel = level;
        }
        else
        {
            Debug.LogError("level" + level + "could not be found");
        }
          
    }

    void unloadCurrentLevel()
    {
        SceneManager.UnloadSceneAsync(currentLevel);
    }
    private void RestartGame()
    {
        UpdateGameState(Enums.GameStates.Pregame);
        unloadCurrentLevel();
    }

    void OnPlayerDeath()
    {
        UpdateGameState(Enums.GameStates.Paused);
    }

    private void OnGameContinue(Enums.AdRewards adRewards)
    {
        switch(adRewards)
        {
            case Enums.AdRewards.Rewarded:
                UpdateGameState(Enums.GameStates.Playing);
                break;
        }       
    }

    private void OnPauseButtonClicked(bool pauseGame)
    {
        if(pauseGame)
        {
            UpdateGameState(Enums.GameStates.Paused);
        }
        else
        {
            UpdateGameState(Enums.GameStates.Playing);
        }
    }

    private void OnEnable()
    {
        InGameUI.onPauseButtonClicked += OnPauseButtonClicked;
        Money.OnRewarded += OnGameContinue;
        UIManager.onGameRestart += RestartGame;
        UIManager.onGameStart += StartGame;
        KillCollider.onPlayerDeath += OnPlayerDeath;
    }
    
    private void OnDisable()
    {
        InGameUI.onPauseButtonClicked -= OnPauseButtonClicked;
        Money.OnRewarded -= OnGameContinue;
        UIManager.onGameRestart -= RestartGame;
        UIManager.onGameStart -= StartGame;
        KillCollider.onPlayerDeath -= OnPlayerDeath;
    }    
}
