using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : Singleton<UIManager>
{
    public static Events.EmptyEvent onGameStart;

    public static Events.EmptyEvent onGameRestart;

    public static Events.EmptyEvent onGameContinue;

    [SerializeField] Camera dummyCamera;
    [SerializeField] MainMenuUI mainMenu;
    [SerializeField] StoreUI storeTemplate;
    [SerializeField] GameOverUI gameOver;
    [SerializeField] HighScoreUI highScore;
    [SerializeField] InGameUI inGame;
    [SerializeField] SettingsUI settings;


    protected override void Awake()
    {
        base.Awake();
        SetBootUI();
    }

    private void SetBootUI()
    {
        dummyCamera.gameObject.SetActive(true);
        mainMenu.gameObject.SetActive(true);
        storeTemplate.gameObject.SetActive(false);
        gameOver.gameObject.SetActive(false);
        highScore.gameObject.SetActive(false);
        inGame.gameObject.SetActive(false);
        settings.gameObject.SetActive(false);
    }

    private void OnMainMenuButtonClick(Enums.MainMenuChoice mainMenuChoice)
    {
        switch(mainMenuChoice)
        {
            case Enums.MainMenuChoice.Play:
                dummyCamera.gameObject.SetActive(false);
                inGame.gameObject.SetActive(true);
                if(onGameStart != null)
                {
                    onGameStart();
                }
                break;

            case Enums.MainMenuChoice.Settings:
                settings.gameObject.SetActive(true);
                break;

            case Enums.MainMenuChoice.Store:
                storeTemplate.gameObject.SetActive(true);
                break;
        }
    }

    private void OnstoreMenuEvent()
    {
        mainMenu.gameObject.SetActive(true);
    }

    void OnPlayerDeath()
    {
        gameOver.gameObject.SetActive(true);
    }

    private void OnGameOverButtonClick(Enums.GameOverChoice gameOverChoice)
    {
        switch(gameOverChoice)
        {
            case Enums.GameOverChoice.QuitRun:
                highScore.gameObject.SetActive(true);
                break;

            case Enums.GameOverChoice.WatchAd:
                Money.instance.DisplayVideoAd();                  
                break;
        }
    }
    private void OnHighScoreButtonClicked()
    {
        SetBootUI();
        if(onGameRestart != null)
        {
            onGameRestart();
        }
    }

    private void OnRewarded(Enums.AdRewards adRewards)
    {
        switch(adRewards)
        {
            case Enums.AdRewards.Skipped:
                highScore.gameObject.SetActive(true);
                break;

            case Enums.AdRewards.Rewarded:
                if(onGameContinue != null)
                {
                    onGameContinue();
                }
                break;
        }
    }

    private void OnSettingsButtonClicked()
    {
        mainMenu.gameObject.SetActive(true);
    }


    private void OnEnable()
    {
        SettingsUI.onSettingsButtonClicked += OnSettingsButtonClicked;
        Money.OnRewarded += OnRewarded;
        HighScoreUI.onHighScoreButtonClicked += OnHighScoreButtonClicked;
        GameOverUI.onGameOverButtonClick += OnGameOverButtonClick;
        KillCollider.onPlayerDeath += OnPlayerDeath;
        MainMenuUI.onMainMenuButtonClick += OnMainMenuButtonClick;
        StoreUI.onStoreMenuEvent += OnstoreMenuEvent;
    }

    private void OnDisable()
    {
        SettingsUI.onSettingsButtonClicked -= OnSettingsButtonClicked;
        Money.OnRewarded -= OnRewarded;
        HighScoreUI.onHighScoreButtonClicked -= OnHighScoreButtonClicked;
        GameOverUI.onGameOverButtonClick -= OnGameOverButtonClick;
        KillCollider.onPlayerDeath -= OnPlayerDeath;
        MainMenuUI.onMainMenuButtonClick -= OnMainMenuButtonClick;
        StoreUI.onStoreMenuEvent -= OnstoreMenuEvent;
    }
}
