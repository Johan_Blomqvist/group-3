using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingWater : MonoBehaviour
{
    [SerializeField][Range (1f, 10f)] float risingSpeed = 0.03f;
    [SerializeField] Player player;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 waterPosition = transform.position;
        Vector3 newPosition = waterPosition;
        newPosition.y += risingSpeed * Time.deltaTime;        
        if(transform.position.y < player.transform.position.y - 20)
        {
            newPosition.y = player.transform.position.y - 20;
        }
        transform.position = newPosition;
    }

    private void OnGameContinue(Enums.AdRewards adRewards)
    {
        switch(adRewards)
        {
            case Enums.AdRewards.Rewarded:
                Vector2 temp = transform.position;
                temp.y -= 10;
                transform.position = temp;
                break;
        }       
    }

    private void OnEnable()
    {
        Money.OnRewarded += OnGameContinue;
    }
    
    private void OnDisable()
    {
        Money.OnRewarded -= OnGameContinue;
    }
}
