using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    //[SerializeField] GameObject spawnPoint;
    [SerializeField][Range (5, 20)] int poolSize = 10;
    [SerializeField] GameObject ObjectPrefab;
    GameObject[] Pool;
    

    // Start is called before the first frame update
    void Awake()
    {
        Pool = new GameObject[poolSize];
        FillPool();

    }

    private void FillPool()
    {
        for (int i = 0; i < poolSize; i++)
        {
            GameObject temp = Instantiate(ObjectPrefab, transform);
            temp.SetActive(false);
            Pool[i] = temp;
        }
    }

    public GameObject[] GetPool()
    {
        return Pool;
    }

    public GameObject UsePooledObject()
    {
        for (int i = 0; i < poolSize; i++)
        {
            if(!Pool[i].activeSelf)
            {
                Pool[i].SetActive(true);
                return Pool[i];
            }
        }

        return null;
    }

    public void ReturnUsedPoolObject(GameObject gameObject)
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        

    }
}
