using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformRandomizer : MonoBehaviour
{
    [SerializeField] Transform levelRandomizerStart;

    [Header ("Platform size settings")]
    [SerializeField][Range (2, 5)] float minDistanceBetweenPlatforms = 2;
    [SerializeField][Range (5, 10)] float maxDistanceBetweenPlatforms = 7;
    [SerializeField] [Range(1, 3)] float minPlatformXScale = 2;
    [SerializeField] [Range(4, 5)] float maxPlatformXScale = 4;
    [SerializeField] [Range(0.3f, 0.5f)] float minPlatformYScale = 0.5f;
    [SerializeField] [Range(0.5f, 1)] float maxPlatformYScale = 1;
    [SerializeField] [Range (5, 10)] int platformsToPlaceAtPlacementTime = 5;
    [SerializeField] [Range(0.1f, 1)]float coinSpawnChance = 0.5f;

    float xMax;
    float xMin;
    Transform lastPlatformPlaced;

    GameObject[] poolArray;
    GameObject[] coinArray;
    [SerializeField] ObjectPool platformPool;
    [SerializeField] ObjectPool CoinPool;
    [SerializeField] GameObject player;
    Camera gameCamera;
    bool initialPlatformPositioned = false;

    // Start is called before the first frame update
    void Awake()
    {
        gameCamera = Camera.main;
        SetUpMoveBoundaries();
    }

    private void SetUpMoveBoundaries()
    {
        xMin = gameCamera.ViewportToWorldPoint(new Vector2(0, 0)).x;
        xMax = gameCamera.ViewportToWorldPoint(new Vector2(1, 0)).x;
    }
    void RandomizeMorePlatforms()
    {
        for(int i = 0; i < platformsToPlaceAtPlacementTime; i++)
        {
            lastPlatformPlaced = SetPlatformPosition(lastPlatformPlaced.position);
            RandomizePlatformSize(lastPlatformPlaced);
        }       
    }

    private void RandomizePlatformSize(Transform lastPlatformPlaced)
    {
        lastPlatformPlaced.gameObject.transform.localScale = new Vector2(UnityEngine.Random.Range(minPlatformXScale, maxPlatformXScale), UnityEngine.Random.Range(minPlatformYScale, maxPlatformYScale));
    }

    private void Start()
    {
        poolArray = platformPool.GetPool();
        coinArray = CoinPool.GetPool();
        lastPlatformPlaced = SetPlatformPosition(levelRandomizerStart.position);
        RandomizeMorePlatforms();
    }

    // Update is called once per frame
    void Update()
    {
        foreach(GameObject platform in poolArray)
        {
            if (platform.activeSelf && platform.transform.position.y <= gameCamera.transform.position.y - 20)
            {
                platform.SetActive(false);
            }
        }
        foreach (GameObject coin in coinArray)
        {
            if (coin.activeSelf && coin.transform.position.y <= gameCamera.transform.position.y - 20)
            {
                coin.SetActive(false);
            }
        }
        if (lastPlatformPlaced.position.y <= gameCamera.transform.position.y + 20)
        {
            RandomizeMorePlatforms();            
        }
    }

    Transform SetPlatformPosition(Vector2 position)
    {
        if(!initialPlatformPositioned)
        {
            GameObject platform = platformPool.UsePooledObject();
            platform.transform.position = position;
            initialPlatformPositioned = true;
            return platform.transform;
        }
        else
        {
            GameObject platform = platformPool.UsePooledObject();
            position.x = gameCamera.transform.position.x;
            platform.transform.position = position + new Vector2(UnityEngine.Random.Range(xMin, xMax), UnityEngine.Random.Range(minDistanceBetweenPlatforms, maxDistanceBetweenPlatforms));
            SetCoinPosition(platform.transform.position);
            return platform.transform;
        }
        
    }

    private void SetCoinPosition(Vector2 position)
    {
        if(UnityEngine.Random.value <= coinSpawnChance)
        {
            GameObject coin = CoinPool.UsePooledObject();
            position.y += 1;
            coin.transform.position = position;
        }
    }

    private void OnGameContinue(Enums.AdRewards adRewards)
    {
        switch(adRewards)
        {
            case Enums.AdRewards.Rewarded:
                InactivatePlatforms();
                InactivateCoins();
                initialPlatformPositioned = false;
                Vector2 temp = new Vector2(0, 2);
                levelRandomizerStart.position = player.transform.position;
                lastPlatformPlaced = SetPlatformPosition(levelRandomizerStart.position);
                RandomizeMorePlatforms();
                break;
        }
    }

    private void InactivatePlatforms()
    {
        foreach (GameObject platform in poolArray)
        {
                platform.SetActive(false);            
        }
    }
    private void InactivateCoins()
    {
        foreach (GameObject coin in coinArray)
        {
            coin.SetActive(false);
        }
    }

    private void OnEnable()
    {
        Money.OnRewarded += OnGameContinue;
    }

    private void OnDisable()
    {
        Money.OnRewarded -= OnGameContinue;
    }
}
