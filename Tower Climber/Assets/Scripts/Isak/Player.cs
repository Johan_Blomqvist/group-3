using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField] AudioClip audioClip;
    AudioSource audioSource;
    public static Events.ScoreUpdateEvent onCoinPickup;
    public static Events.ScoreUpdateEvent FinalScoreEvent;
 
    Rigidbody2D rigidbody2D;
    CircleCollider2D collider2D;

    [SerializeField][Range (0.1f, 1)] float inputSensitivity = 1;
    [SerializeField] LayerMask layerMask;
    [SerializeField][Range (0.1f, 3)] float jumpForce = 3;

    private int score = 0;
    bool isPaused = false;
    private float xMin;
    private float xMax;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void Start()
    {

        SetUpMoveBoundaries();
        rigidbody2D = GetComponent<Rigidbody2D>();
        collider2D = GetComponent<CircleCollider2D>();
    }

    private void SetUpMoveBoundaries()
    {
        Camera gameCamera = Camera.main;
        xMin = gameCamera.ViewportToWorldPoint(new Vector2(0, 0)).x;
        xMax = gameCamera.ViewportToWorldPoint(new Vector2(1, 0)).x;
    }
    void Update()
    {
        if (!isPaused)
        {
            Move();
        }
    }

    private void Move()
    {
        Vector2 accelerometer = new Vector2();
        accelerometer.x = Input.acceleration.x * inputSensitivity;
        Vector2 newPos = gameObject.transform.position;
        newPos += accelerometer;
        if (newPos.x <= xMin)
        {
            newPos.x = xMax;
        }
        else if(newPos.x >= xMax)
        {
            newPos.x = xMin;
        }
        transform.position = newPos;       
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "Platform")
        {
            gameObject.GetComponent<Animator>().SetBool("isJumping", true);          
        }
    }

    public void StopJumping()
    {
        gameObject.GetComponent<Animator>().SetBool("isJumping", false);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        audioSource.PlayOneShot(audioClip);
    }
    
    private void OnGameStateChange(Enums.GameStates gameState)
    {
        switch (gameState)
        {
            case Enums.GameStates.Paused:
                isPaused = true;
                break;

            case Enums.GameStates.Playing:
                isPaused = false;
                break;

            case Enums.GameStates.Pregame:
                isPaused = true;
                break;
        }
    }

    private void OnEnable()
    {
        GameOverUI.onGameOverButtonClick += OnGameOverButtonClick;
        GameManager.onGameStateChange += OnGameStateChange;
    }

    private void OnGameOverButtonClick(Enums.GameOverChoice gameOverChoice)
    {
        switch(gameOverChoice)
        {
            case Enums.GameOverChoice.QuitRun:
                if(FinalScoreEvent != null)
                {
                    FinalScoreEvent(score);
                }
                break;
        }
    }

    private void OnDisable()
    {
        GameOverUI.onGameOverButtonClick -= OnGameOverButtonClick;
        GameManager.onGameStateChange -= OnGameStateChange;
    }
}
