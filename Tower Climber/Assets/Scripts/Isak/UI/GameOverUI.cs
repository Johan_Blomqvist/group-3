using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class GameOverUI : MonoBehaviour
{

    [SerializeField] TextMeshProUGUI text;
    [SerializeField] Button yesButton;
    [SerializeField] Button noButton;
    [SerializeField] Button button;

    public static Events.GameOverMenuEvent onGameOverButtonClick;
    bool watchedAd = false;

    public void ContinueButton()
    {
        watchedAd = true;
        if(onGameOverButtonClick != null)
        {
            onGameOverButtonClick(Enums.GameOverChoice.WatchAd);
        }
        gameObject.SetActive(false);
    }

    public void QuitButton()
    {
        if (onGameOverButtonClick != null)
        {
            onGameOverButtonClick(Enums.GameOverChoice.QuitRun);
        }
        gameObject.SetActive(false);
    }

    private void OnEnable()
    {       
        if(watchedAd)
        {
            text.text = "Game Over";
            yesButton.gameObject.SetActive(false);
            noButton.gameObject.SetActive(false);
            button.gameObject.SetActive(true);
        }
        else
        {
            text.text = "Game Over";
            text.text += "\n";
            text.text += "Watch an Ad to continue run?";
            yesButton.gameObject.SetActive(true);
            noButton.gameObject.SetActive(true);
            button.gameObject.SetActive(false);
        }
    }

    private void Awake()
    {
        UIManager.onGameRestart += OnGameRestart;
    }

    private void OnGameRestart()
    {
        watchedAd = false;
    }

    private void OnDestroy()
    {
        UIManager.onGameRestart -= OnGameRestart;
    }
}
