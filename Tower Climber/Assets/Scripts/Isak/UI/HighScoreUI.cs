using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HighScoreUI : MonoBehaviour
{

    public static Events.EmptyEvent onHighScoreButtonClicked;

    [SerializeField] TextMeshProUGUI leaderboardText;

    public void ContinueButton()
    {
        if(onHighScoreButtonClicked != null)
        {
            onHighScoreButtonClicked();
        }
        gameObject.SetActive(false);
    }
    
    void UpdateLeaderboardText(List<int> scoreList)
    {
        int tempInt;
        TextMeshProUGUI temp = new TextMeshProUGUI();
       
        temp.text = string.Empty;
        //temp = int;
        /*for(int i = 0; i < scoreList.Count; i++)
        {
            temp.text += (i + 1).ToString() + "st: ";
            temp.text += scoreList[i].ToString();
            temp.text += "\n";
        }*/
        for (int i = 0; i < scoreList.Count; i++)
        {
            for(int j = i + 1; j < scoreList.Count; j++)
            {
                Debug.Log(scoreList.Count);
                Debug.Log(scoreList[i] + "i:innan");
                Debug.Log(scoreList[j] + "j:innan");

                if (scoreList[j] > scoreList[i])
                {
                  
                    tempInt = scoreList[i];
                    scoreList[i] = scoreList[j];
                    scoreList[j] = tempInt;

                    //scoreList[0] = scoreList[i];
                    scoreList[i].ToString();
                    scoreList[j].ToString();

                    //temp.text += scoreList[i].ToString() + "\n";
                    

                    
                }
                
                Debug.Log(scoreList[i] + "i");
                Debug.Log(scoreList[j] + "j");
            }
        }

        /*foreach (temp.text in leaderboardText.text)
        {
            leaderboardText.text();
        }*/

        /*if(scoreList.Count == 1)
        {
            temp.text = scoreList[0].ToString();
        }*/

        //temp.text = scoreList[0].ToString();

        for (int i = 0; i < scoreList.Count; i++)
        {
            temp.text += (i + 1).ToString() + "st: ";
            temp.text += scoreList[i].ToString();
            temp.text += "\n";
        }

        leaderboardText.text = temp.text;

        //leaderboardText.text = rankString;
    }

    private void Awake()
    {
        LeaderBoard.leaderboardEvent += UpdateLeaderboardText;
    }

    private void OnDestroy()
    {
        LeaderBoard.leaderboardEvent -= UpdateLeaderboardText;
    }
}
