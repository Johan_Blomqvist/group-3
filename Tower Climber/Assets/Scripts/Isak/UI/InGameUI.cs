using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InGameUI : MonoBehaviour
{

    public static Events.PauseButtonEvent onPauseButtonClicked;

    [SerializeField] Button pauseButton;
    [SerializeField] TextMeshProUGUI scoreText;
    public bool isPaused = false;

    private void Awake()
    {
        UpdateScoreText(0);
    }

    void UpdateScoreText(int score)
    {
        scoreText.text = score.ToString();
    }
    public void PauseButton()
    {
        isPaused = !isPaused;
        if(isPaused)
        {
            pauseButton.GetComponentInChildren<TextMeshProUGUI>().text = "Resume";
        }
        else
        {
            pauseButton.GetComponentInChildren<TextMeshProUGUI>().text = "Pause";
        }
        if (onPauseButtonClicked != null)
        {
            onPauseButtonClicked(isPaused);
        }
    }

    private void OnGameRestart()
    {
        UpdateScoreText(0);
    }

    private void OnEnable()
    {
        UIManager.onGameRestart += OnGameRestart;
        Player.onCoinPickup += UpdateScoreText;
    }

    private void OnDestroy() 
    {
        UIManager.onGameRestart -= OnGameRestart;
        Player.onCoinPickup -= UpdateScoreText;
    }
}
