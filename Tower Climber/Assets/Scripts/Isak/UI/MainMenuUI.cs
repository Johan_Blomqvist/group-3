using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuUI : MonoBehaviour
{

    public static Events.MainMenuEvent onMainMenuButtonClick;

    public void SettingsButton()
    {
        if(onMainMenuButtonClick != null)
        {
            onMainMenuButtonClick(Enums.MainMenuChoice.Settings);
        }
        gameObject.SetActive(false);
    }

    public void StoreButton()
    {
        if (onMainMenuButtonClick != null)
        {
            onMainMenuButtonClick(Enums.MainMenuChoice.Store);
        }
        gameObject.SetActive(false);
    }

    public void StartButton()
    {
        if (onMainMenuButtonClick != null)
        {
            onMainMenuButtonClick(Enums.MainMenuChoice.Play);
        }
        gameObject.SetActive(false);
    }
}
