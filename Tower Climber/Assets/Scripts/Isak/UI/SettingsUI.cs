using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsUI : MonoBehaviour
{

    public static Events.EmptyEvent onSettingsButtonClicked;

    public void ReturnButton()
    {
        if(onSettingsButtonClicked != null)
        {
            onSettingsButtonClicked();
        }
        gameObject.SetActive(false);
    }
}
