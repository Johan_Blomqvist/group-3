using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreUI : MonoBehaviour
{

    public static Events.EmptyEvent onStoreMenuEvent;
    public void ReturnButton()
    {
        if(onStoreMenuEvent != null)
        {
            onStoreMenuEvent();
        }
        gameObject.SetActive(false);
    }
}
