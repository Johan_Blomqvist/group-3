using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Falling : MonoBehaviour
{
    [SerializeField] private float timeBetweenFalls;
    [SerializeField] private GameObject firstActiveGameObject;
    private GameObject currentCube;


    public void Start()
    {
        startFallOfBlocks();
    }

    public void startFallOfBlocks()
    {
        InvokeRepeating("DestroyBlock", 5f, timeBetweenFalls);
    }

    private void DestroyBlock()
    {

        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            if (gameObject.transform.GetChild(i).gameObject.activeSelf == true)
            {
                currentCube = gameObject.transform.GetChild(i).gameObject;
                
                break;
            }

        }
        LeanTween.moveY(currentCube, -5, 1f).setEaseInSine().setOnComplete(disableCube);
    }

    private void disableCube()
    {
        
        currentCube.SetActive(false);
    }
}
