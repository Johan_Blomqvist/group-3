using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumpinput : MonoBehaviour
{
    public TiltMovement TInput;

    bool jump = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount >= 1)
        {
            
            jump = true;
        }
    }
    private void FixedUpdate()
    {
        TInput.Jump(jump);
        jump = false;
    }
}
