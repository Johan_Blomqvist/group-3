using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;

public class LeaderBoard : Singleton<LeaderBoard>
{

    public static Events.LeaderboardEvent leaderboardEvent;

    //added variables
    List<int> HighscoreList;
    //

    private Transform entryContainer;
    private Transform entryTemplate;
    //private List<HighscoreEntry> highscoreEntryList;
    private List<Transform> highscoreEntryTransformList;
    protected override void Awake()
    {
        base.Awake();
        HighscoreList = new List<int>();

        //entryContainer = transform.Find("highscoreEntryContainer");
        //entryTemplate = entryContainer.Find("highscoreEntryTemplate");

        //entryTemplate.gameObject.SetActive(false);

        

        //string jsonString = PlayerPrefs.GetString("highscoreTable");
        //Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);

        //sort Entry
        //for (int i = 0; i < highscores.highscoreEntryList.Count; i++)
        {
           // for(int j = i + 1; j < highscores.highscoreEntryList.Count; j++)
            {
               // if (highscores.highscoreEntryList[j].score > highscores.highscoreEntryList[i].score)
                {
                   // HighscoreEntry tmp = highscores.highscoreEntryList[i];
                   // highscores.highscoreEntryList[i] = highscores.highscoreEntryList[j];
                   // highscores.highscoreEntryList[j] = tmp;
                }
            }
        }
        //highscoreEntryTransformList = new List<Transform>();
        //foreach(HighscoreEntry highscoreEntry in highscores.highscoreEntryList)
        {
            //CreateHighscoreEntryTransform(highscoreEntry, entryContainer, highscoreEntryTransformList);
        }
        
    }

    private void CreateHighscoreEntryTransform(HighscoreEntry highscoreEntry, Transform container, List<Transform> transformList)
    {
            float templateHeight = 30f;
            Transform entryTransform = Instantiate(entryTemplate, container);
            RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
            entryRectTransform.anchoredPosition = new Vector2(0, -templateHeight * transformList.Count);
            entryTransform.gameObject.SetActive(true);

            int rank = transformList.Count + 1;
            string rankString;
            switch (rank)
            {
                default:
                    rankString = rank + "TH"; break;

                case 1: rankString = "1ST"; break;
                case 2: rankString = "2ST"; break;
                case 3: rankString = "3RD"; break;

            }

            entryTransform.Find("PosText").GetComponent<Text>().text = rankString;

        int score = highscoreEntry.score;

            entryTransform.Find("ScoreText").GetComponent<Text>().text = score.ToString();

        string name = highscoreEntry.name;
            entryTransform.Find("NameText").GetComponent<Text>().text = name;

        transformList.Add(entryTransform);
    }

    private void AddHighscoreEntry(int score, string name)
    {
        //score = PlayerPrefs.GetInt("Coin");
        // creat HighscoreEntry
        HighscoreEntry highscoreEntry = new HighscoreEntry { score = score, name = name };

        //Load saved Highscores
        string jsonString = PlayerPrefs.GetString("highscoreTable");
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);

        //Add new entry to Highscores
        highscores.highscoreEntryList.Add(highscoreEntry);

        //save update Highscores
        string json = JsonUtility.ToJson(highscores);
        PlayerPrefs.SetString("highscoreTable", json);
        PlayerPrefs.Save();
    }

    private class Highscores
    {
        public List<HighscoreEntry> highscoreEntryList;
    }
    [System.Serializable]
    private class HighscoreEntry
    {
        
        public int score;
        
        public string name;
    }

    private void OnEnable()
    {
        Player.FinalScoreEvent += AddPlayerScoreToLeaderboard;
    }

    private void AddPlayerScoreToLeaderboard(int score)
    {
        HighscoreList.Add(score);
        if(leaderboardEvent != null)
        {
            leaderboardEvent(HighscoreList);
        }

        AnalyticsResult analyticsResult = Analytics.CustomEvent("Highscore", new Dictionary<string, object>
        {
            {"Score", score }
        });

        Debug.Log("ScoreanalyticsResult" + analyticsResult);
           

    }

    private void OnDisable()
    {
        Player.FinalScoreEvent -= AddPlayerScoreToLeaderboard;
    }
}
