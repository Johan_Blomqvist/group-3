using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelLoader : MonoBehaviour
{
    private const float PLAYER_DISTANCE_SPAWN_LEVEL_PART = 50f;
    [SerializeField] private Transform StartPart;
    [SerializeField] private List<Transform> levelPartsList;
    private int asdas = 10;
    [SerializeField] private Transform player;

    private Vector3 lastEndPosition;
    
    private void Awake()
    {
        lastEndPosition = (StartPart.Find("End").position);

        int startingSpawnLevelParts = 1;
        for(int i = 0; i < startingSpawnLevelParts; i++)
        {
            SpawnLevelPart();
        }

    }
    private void Update()
    {
        if(Vector3.Distance(player.position, lastEndPosition) < PLAYER_DISTANCE_SPAWN_LEVEL_PART)
        {
            SpawnLevelPart();
        }
    }
    private void SpawnLevelPart()
    {
        Transform chosenLevelPart = levelPartsList[Random.Range(0, levelPartsList.Count)];
        Transform lastlevelPartTransform = SpawnLevelPart(chosenLevelPart, lastEndPosition);
        lastEndPosition = lastlevelPartTransform.Find("End").position;
    }
    private Transform SpawnLevelPart(Transform levelPart, Vector3 spawnPosition)
    {
        Transform levelPartTransform = Instantiate(levelPart, spawnPosition, Quaternion.identity);
        return levelPartTransform;
        
    }

    
}
