using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TiltMovement : MonoBehaviour
{
    private float JumpForce = 7f;
    Rigidbody2D rb;
    
    //float moveSpeed = 50f;
    [SerializeField] float minXPos = 1;
    [SerializeField] float maxXPos = 1;
    [SerializeField] [Range(0.1f, 1)] float inputSensitivity = 1;

    [SerializeField] private LayerMask WhatIsGround;
    [SerializeField] private Transform GroundCheck;
    const float GroundedRadius = .2f;
    private bool Grounded;

    //public UnityEvent OnLandEvent;
    //private bool jumping = false;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        
    }
    private void Awake()
    {
        
        
    }
    // Update is called once per frame
    void Update()
    {
        Move();

    }
    private void Move()
    {
        Vector2 accelerometer = new Vector2();
        accelerometer.x = Input.acceleration.x * inputSensitivity;
        Vector2 newPos = gameObject.transform.position;
        newPos += accelerometer;
        newPos.x = Mathf.Clamp(newPos.x, minXPos, maxXPos);
        transform.position = newPos;
    }

    private void FixedUpdate()
    {
        //bool wasGrounded = Grounded;
        Grounded = false;
        
        
        
        Collider2D[] colliders = Physics2D.OverlapCircleAll(GroundCheck.position, GroundedRadius, WhatIsGround);
        for (int i = 0; i <colliders.Length; i++)
        {
            if(colliders[i].gameObject != gameObject)
            {
                Grounded = true;
                
                
            }
        }

    }
    public void Jump(bool jump)
    {

        if (Grounded && jump)
        {
            Debug.Log("working");
            Grounded = false;
            rb.AddForce(new Vector2(0f, JumpForce), ForceMode2D.Impulse);
        }
    }
    

    
    
    
}
