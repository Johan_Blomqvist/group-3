using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{

    List<HighscoreList> highscoreList;
    HighscoreList currentPlayerScore; // ------
    
    private class HighscoreList
    {
        public int score;
    }
    void UpdateCurrentPlayerScore(int score)
    {
        currentPlayerScore.score = score;
    }
    void UpdateHighScoreList(HighscoreList playerScore)
    {
        highscoreList.Add(playerScore);
    }
}
